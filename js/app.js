// Import the functions you need from the SDKs you need
import { initializeApp } from
"https://www.gstatic.com/firebasejs/10.11.1/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
import { getDatabase,onValue,ref as refS,set,get, update, child,remove}
from "https://www.gstatic.com/firebasejs/10.11.1/firebase-database.js"
//firebase.google.com/f irebasejs/10.11.1/firebase-database.js";
// Your web app's Firebase configuration
const firebaseConfig = {
apiKey: "AIzaSyBK0nHrSlFrniSyijCZcXLjT2vniDS3-t0",
authDomain: "webcrud-b3ccb.firebaseapp.com",
databaseURL: "https://webcrud-b3ccb-default-rtdb.firebaseio.com",
projectId: "webcrud-b3ccb",
storageBucket: "webcrud-b3ccb.appspot.com",
messagingSenderId: "825180979694",
appId: "1:825180979694:web:7ff060c0b1f450d8254eff"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
// declarar unas variables global
var numSerie = "";
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";
// funciones
function leerInputs(){
    numSerie = document.getElementById('txtNumSerie').value;
    marca = document.getElementById('txtMarca').value;
    modelo = document.getElementById('txtModelo').value;
    descripcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
    var mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => {
        mensajeElement.style.display = 'none';
    }, 1000);
}
document.getElementById('uploadButton').addEventListener('click', (event) => {
    event.preventDefault();
    subirimg();
});

function subirimg() {
    leerInputs();
    set(refS(db, 'Automoviles/' + numSerie), {
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        actualizarFilaTabla({
            numSerie: numSerie,
            marca: marca,
            modelo: modelo,
            descripcion: descripcion,
            urlImag: urlImag
        });
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
}
// Agregar producto a la base de datos
document.getElementById('btnAgregar').addEventListener('click', insertarProducto);

function insertarProducto() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Faltaron datos por capturar");
        return;
    }
    set(refS(db, 'Automoviles/' + numSerie), {
        numSerie: numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        alert("Se agregó con éxito");
        Listarproductos();
    }).catch((error) => {
        alert("Ocurrió un error");
    });
}

function Listarproductos() {
    const dbRef = refS(db, 'Automoviles');
    const tbody = document.getElementById('tablaProductos').querySelector('tbody');
    tbody.innerHTML = '';

    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const data = childSnapshot.val();
            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImg;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);

            agregarFilaTabla(data);
        });
    }, { onlyOnce: true });
}
function agregarFilaTabla(data) {
    const tbody = document.getElementById('tablaProductos').querySelector('tbody');
    const fila = document.createElement('tr');
    fila.setAttribute('id', 'fila-' + data.numSerie);
    
    fila.innerHTML = `
        <td>${data.numSerie}</td>
        <td>${data.marca}</td>
        <td>${data.modelo}</td>
        <td>${data.descripcion}</td>
        <td><img src="${data.urlImag}" width="100"></td>
    `;
    tbody.appendChild(fila);
}

function buscarAutomovile() {
    numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("Falto capturar el numero de serie");
        return;
    }
    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
        if (snapshot.exists()) {
            const data = snapshot.val();
            marca = data.marca;
            modelo = data.modelo;
            descripcion = data.descripcion;
            urlImag = data.urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("No se encontró el registro");
        }
    });
}

const imageInput = document.getElementById('imageInput');
const txtUrl = document.getElementById('txtUrl');

imageInput.addEventListener('change', (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        txtUrl.value = reader.result;
        // Mostrar la imagen en tiempo real
        const previewImage = document.getElementById('previewImage');
        previewImage.src = reader.result;
    };
});

// Se agrego la funcion actualizar
document.getElementById('btnActualizar').addEventListener('click', actualizarAutomovil);

function actualizarAutomovil() {
    leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la información.");
        return;
    }
    update(refS(db, 'Automoviles/' + numSerie), {
        numSerie: numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizó con éxito.");
        actualizarFilaTabla({
            numSerie: numSerie,
            marca: marca,
            modelo: modelo,
            descripcion: descripcion,
            urlImag: urlImag
        });
    }).catch((error) => {
        mostrarMensaje("Ocurrió un error: " + error);
    });
}
function actualizarFilaTabla(data) {
    const fila = document.getElementById('fila-' + data.numSerie);
    if (fila) {
        fila.innerHTML = `
            <td>${data.numSerie}</td>
            <td>${data.marca}</td>
            <td>${data.modelo}</td>
            <td>${data.descripcion}</td>
            <td><img src="${data.urlImag}" width="100"></td>
        `;
    }
}
// Se agrego la funcion elimina eliminar Automovil
function eliminarAutomovil() {
let numSerie= document.getElementById('txtNumSerie').value.trim();
if (numSerie === "") {
mostrarMensaje("No se ingresó un Codigo válido.");
return;
}
const dbref = refS(db);
get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {

if (snapshot.exists()) {
remove(refS(db, 'Automoviles/' + numSerie))
.then(() => {
mostrarMensaje("Producto eliminado con éxito.");
limpiarInputs();
Listarproductos();
})
.catch((error) => {
mostrarMensaje("Ocurrió un error al eliminar el producto: " + error);
});
} else {
limpiarInputs();
mostrarMensaje("El producto con ID " + numSerie + " no existe.");
}
});
Listarproductos();
}
function escribirInputs(){
document.getElementById('txtMarca').value= marca;
document.getElementById('txtModelo').value= modelo;
document.getElementById('txtDescripcion').value= descripcion;
document.getElementById('txtUrl').value= urlImag;
}
function limpiarInputs(){
document.getElementById('txtNumSerie').value= '';
document.getElementById('txtMarca').value= '';
document.getElementById('txtModelo').value= '';
document.getElementById('txtDescripcion').value= ''; // se cambio a txtDescripcion con D mayuscula
document.getElementById('txtUrl').value='';

}

// se codificaron los metodos
const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click',buscarAutomovile);
const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click',eliminarAutomovil);
const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);